import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { Provider } from "react-redux";

import {reducer} from './reducer/';
import {watcherSaga} from './sagas/';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

let store = createStore(reducer,compose(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(watcherSaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
