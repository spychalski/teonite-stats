import React, { Component } from "react";
import "./App.css";

import { connect } from "react-redux";
import SelectBox from "./components/SelectBox";
import Table from "./components/Table";

class Stats extends Component {

  constructor(props) {
    super(props);
    this.state = {
      removedValue:false,
      clearAll:false,
    };
  }

  onAuthorUpdated = (param) => {

    this.setState({

      removedValue:param['removedValue'],
      clearAll : param['clear']

    }, () => {

        if(param['selectAll']){

          if(param['selectedAuthors']==='all'){
            if(param['clear']){
              this.setState({clearAll : false}, () => {this.props.onRequestAllAuthors();});
            }else{
              this.setState({clearAll : true}, () => {this.props.onRequestAllAuthors();});
            }
          }else{
            this.setState({clearAll : false}, () => {this.props.onRequestAllAuthors();});
          }

        }else{
          if(!param['clear']){
            this.props.onRequestAuthor(param['selectedAuthors']);
          }
        }
    });
  }

  afterClearAll = () => {
    this.setState({
      removedValue:false,
      clearAll : false
    });
  }

  componentDidMount = () => {
    this.props.onRequestInit()
  }

  render() {

    const {author, error, stats} = this.props;

    return (
      <div className="App">
        {error && <p style={{ color: "red" }}>błąd przy pobieraniu danych z API, może to dodatek Allow-Control-Allow-Origin: * do przeglądarki CHROME</p>}

        {author &&
          <SelectBox selected={this.onAuthorUpdated.bind(this)} title='Autorzy' authors={author}  />
        }

        <Table stats={stats} removedValue={this.state.removedValue} clearAll={this.state.clearAll} afterClearAll={this.afterClearAll.bind(this)}/>

      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    fetching: state.fetching,
    fetched: state.fetched,
    author: state.author,
    error: state.error,
    stats: state.stats,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRequestInit: () => dispatch({ type: "API_CALL_REQUEST" }),
    onRequestAuthor: (param) => dispatch({ type: "API_CALL_AUTHORS", authorName:param }),
    onRequestAllAuthors: () => dispatch({ type: "API_CALL_ALL_AUTHORS" })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Stats);
