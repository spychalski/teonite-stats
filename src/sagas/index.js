import { takeLatest, call, put } from "redux-saga/effects";
import axios from "axios";

export function* watcherSaga() {
  yield takeLatest("API_CALL_REQUEST", workerSaga);
  yield takeLatest("API_CALL_AUTHORS", workerSaga2);
  yield takeLatest("API_CALL_ALL_AUTHORS", workerSaga3);

}


function fetchAuthor() {
  return axios({
    method: "get",
    url:"http://127.0.0.1:8080/authors"
  });
}


function* workerSaga() {
  try {
    const response = yield call(fetchAuthor);
    const author = response.data;
    yield put({ type: "API_CALL_SUCCESS", author });

  } catch (error) {
    yield put({ type: "API_CALL_FAILURE", error });
  }
}



function fetchAuthor2(authorName) {
  return axios({
    method: "get",
    url:"http://127.0.0.1:8080/stats/"+authorName
  });
}

function* workerSaga2(param) {
  try {
    const response = yield call(fetchAuthor2,param.authorName);
    const stats = response.data;
    yield put({ type: "API_CALL_AUTHORS_SUCCESS", stats });

  } catch (error) {
    yield put({ type: "API_CALL_FAILURE", error });
  }
}



function fetchAuthor3() {
  return axios({
    method: "get",
    url:"http://127.0.0.1:8080/stats/"
  });
}


function* workerSaga3() {
  try {
    const response = yield call(fetchAuthor3);
    const stats = response.data;
    yield put({ type: "API_CALL_ALL_AUTHORS_SUCCESS", stats });

  } catch (error) {
    yield put({ type: "API_CALL_FAILURE", error });
  }
}
