import React, { Component } from 'react';
import Select from 'react-select';

const Checkbox = props => <input type="checkbox" {...props} />;
const Label = props => <label {...props} />;

class SelectBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      listItems:[],
      listItemsAll:[],
      selectAll:false
    }
  }

  handleChange = (newValue: any, actionMeta: any) => {

    let tempArr=[];
    if(actionMeta.action==='remove-value'){

      if(this.state.selectAll){

        this.setState(state => ({ listItemsAll: [] }));
        tempArr['selectAll']=false;
        tempArr['clear']=true;
        tempArr['removedValue']=false;
        tempArr['selectedAuthors']=null;
        this.setState(state => ({ selectAll: false }));

      }else{

        this.setState(state => ({ listItemsAll: [] }));
        tempArr['selectAll']=false;
        tempArr['clear']=false;
        tempArr['removedValue']=true;
        tempArr['selectedAuthors']=actionMeta.removedValue.value;
      }
    }else if(actionMeta.action==='select-option'){
      tempArr['selectAll']=false;
      tempArr['clear']=false;
      tempArr['removedValue']=false;
      tempArr['selectedAuthors']=actionMeta.option.value;
    }else if(actionMeta.action==='clear'){
      this.setState(state => ({ listItemsAll: [] }));
      tempArr['selectAll']=false;
      tempArr['clear']=true;
      tempArr['removedValue']=false;
      tempArr['selectedAuthors']=null;

      this.setState(state => ({ selectAll: false }));

    }

      this.props.selected(tempArr);

  };



  selectAll = () => {

    let allAuthorsArr=[];
    let tempArr=[];

    if(this.state.selectAll){

      this.setState(state => ({ selectAll: false }));

      allAuthorsArr=[];
      tempArr['selectAll']=false;
      tempArr['clear']=true;
      tempArr['removedValue']=false;
      tempArr['selectedAuthors']=null;
    }else{
      tempArr['selectAll']=true;
      tempArr['clear']=true;
      tempArr['removedValue']=false;
      tempArr['selectedAuthors']='all';

      this.setState(state => ({ selectAll: true }));
      allAuthorsArr=[];
      Object.keys(this.props.authors).map((key) =>
        allAuthorsArr.push({ value: key, label: this.props.authors[key] })
      )
    }

    this.props.selected(tempArr);
    this.setState(state => ({ listItemsAll: allAuthorsArr }));

}

  render() {

    //do not create another option list on each call
    if(this.state.listItems.length===0){
        Object.keys(this.props.authors).map((key) =>
          this.state.listItems.push({ value: key, label: this.props.authors[key] })
        )
    }
    return (
      <div style={{width:'50%', margin:'0 auto'}} >
        <h3>{this.props.title}</h3>

        {this.state.selectAll &&
          <Select
            isSearchable="false"
            isMulti
            name="authors"
            options={this.state.listItems}
            value={this.state.listItemsAll}
            className="basic-multi-select"
            classNamePrefix="select"
            onChange={this.handleChange}
          />
        }

        {!this.state.selectAll &&
          <Select
            isSearchable="false"
            isMulti
            name="authors"
            options={this.state.listItems}

            className="basic-multi-select"
            classNamePrefix="select"
            onChange={this.handleChange}
          />
        }

        <br/>

        <Label style={{ float: 'right'}}>
          <Checkbox
            onChange={this.selectAll}
            checked={this.state.selectAll}
          />
          Select All
        </Label>

        <br/><br/>

      </div>
    );
  }
}

export default SelectBox;
