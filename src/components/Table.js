import React, { Component } from 'react';
import ReactTable from 'react-table'
import 'react-table/react-table.css'

class Table extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stats : [],
    };
  }

  componentDidUpdate(prevProps) {

    if (this.props.stats !== prevProps.stats) {

      let tempArr=[];
      let resultArr=[];
      let removedValue = this.props.removedValue;

      Object.keys(this.props.stats).map((key) =>
        tempArr.push({ word: key, quantity: this.props.stats[key]})
      )

      //comparison of tempArr and this.state.stats
      if(prevProps.stats){

        this.state.stats.map(function(value, index){
          tempArr.map(function(value2, index2){
            if(value.word === value2.word){
              if(!removedValue){
                resultArr.push({ word: value.word, quantity: (value.quantity + value2.quantity)});
              }else{
                resultArr.push({ word: value.word, quantity: (value.quantity - value2.quantity)});
              }
            }
          })
        })
      }else{
        resultArr = tempArr;
      }

      this.setState({
        stats: resultArr
      });

      this.forceUpdate();

    }else{

        if ( this.props.clearAll && this.state.stats.length>0) {

          let resultArr=[];
          this.state.stats.map(function(value, index){
            resultArr.push({ word: value.word, quantity: 0});
          });

          this.setState({
            stats: resultArr
          });
          this.props.afterClearAll();

        }

    }
  }

  render() {
    const columns = [{
      Header: 'Words',
      accessor: 'word'
    }, {
      Header: 'Count',
      accessor: 'quantity',
    }]


    return(

      <div style={{width:'50%', margin:'0 auto'}} >
        <ReactTable
          data={this.state.stats}
          columns={columns}
        />
      </div>
    )

  }

}

export default Table;
