import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import Stats from './Stats';


class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
        <header className="App-header">
          <h1 className="App-title">go to <Link to="/stats">STATS</Link> to get authors stat</h1>
        </header>
        <Route exact path="/stats" component={Stats} />
        </div>
      </Router>
    );
  }
}

export default App;
