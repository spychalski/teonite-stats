// action types
const API_CALL_REQUEST = "API_CALL_REQUEST";
const API_CALL_SUCCESS = "API_CALL_SUCCESS";
const API_CALL_FAILURE = "API_CALL_FAILURE";

const API_CALL_AUTHORS = "API_CALL_AUTHORS"
const API_CALL_AUTHORS_SUCCESS = "API_CALL_AUTHORS_SUCCESS"

const API_CALL_ALL_AUTHORS = "API_CALL_ALL_AUTHORS"
const API_CALL_ALL_AUTHORS_SUCCESS = "API_CALL_ALL_AUTHORS_SUCCESS"


// reducer with initial state
const initialState = {
  author: null,
  error: null,
  authorName: null
};

export function reducer(state = initialState, action) {

  switch (action.type) {

    case API_CALL_REQUEST:

        return { ...state, error: null };

    case API_CALL_AUTHORS:

        return { ...state, error: null, authorName:action.authorName };

    case API_CALL_SUCCESS:

        return { ...state, author: action.author };

    case API_CALL_AUTHORS_SUCCESS:

        return { ...state, stats: action.stats };

    case API_CALL_ALL_AUTHORS:

        return { ...state, error: null };

    case API_CALL_ALL_AUTHORS_SUCCESS:

        return { ...state, stats: action.stats };

    case API_CALL_FAILURE:

        return { ...state, author: null, error: action.error };

    default:
      return state;
  }
}
